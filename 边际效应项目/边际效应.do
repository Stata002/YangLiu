
*******************************************************
*                      边际效应                       *
*******************************************************

*- 3.1 基础案例
   /*
   我们以研究妇女工资的影响因素为例对这两个命令的基本使用方法进行说明。
   使用 stata 的自带数据 nlsw88.dta (1988年美国妇女小时工资)，
   以 wage (妇女的小时工资) 作为被解释变量、
   以 ttl_exp (总工作年限)、 tenure (当前工作年限)、 
   collgrad (是否大学毕业) 、south (是否在南部地区)、 
   smsa (是否在SMSA地区)、 union (是否工会成员)、 
   race (种族类别) 作为解释变量建立线性归回模型。
   race 变量为 类别变量，它包括三个类别，
   分别为 white、black、other，
   我们可以使用 因子变量 的语法格式，
   在变量前面加上前缀 i. 生成虚拟变量 (i.race)，
   基准组为第一个类别 white。stata 中的回归命令和结果如下所示：
   */

  sysuse "nlsw88.dta", clear
  
  reg wage ttl_exp tenure collgrad south smsa union i.race
  
  /*
  回归结果显示：妇女种族为 black 的系数值为 -0.831 并在1%的水平上显著；
  妇女种族为 other 的系数值为 0.427 但在统计上不显著。
  结果表明：妇女的种族 race 变量对妇女的小时工资具有影响作用。
  因此，我们想进一步的了解各个类别的种族（white, black, other）
  对妇女的小时工资的边际效应分别是多少。
  于是，我们使用 margins 命令附加 atmeans 的选项
  就可以计算 race 各个类别在 所有变量的均值处 的 边际效应。
  stata 命令和结果如下所示：
  */
  
  margins i.race, atmeans    //前缀 i. 可省略不写
  
  /*
  margins 命令的计算结果显示：white 对妇女的小时工资的边际效应为 7.801，
  表明当其他变量处于均值水平时，白种人妇女的小时工资的平均值为 7.801；
  black 对妇女的小时工资的边际效应为 6.970，表明当其他变量处于均值水平时，
  黑种人妇女的小时工资的平均值为 6.970；
  other 对妇女的小时工资的边际效应为 8.228，
  表明当其他变量处于均值水平时，其他种族的妇女的小时工资的平均值为 8.228。
  
  我们还想将边际效应的计算结果用图的形式表示。
  使用 marginsplot 命令就可以很方便的实现这个想法。
  stata 命令如下所示：
  */
  
  marginsplot
  
*- 3.2 交乘项案例

   *- 我们仍以研究妇女工资的影响因素为例
   *- 对计算交乘项的边际效应的使用方法进行说明。
   
*- 3.2.1 类别变量与类别变量交乘

   /*
   使用 stata 的自带数据 nlsw88.dta (1988年美国妇女小时工资)，
   以 wage (妇女的小时工资) 作为被解释变量、
   以 ttl_exp (总工作年限)、 tenure (当前工作年限)、 
   collgrad (是否大学毕业) 、 race (种族类别)、 
   race (种族类别) 与 collgrad (是否大学毕业) 的 交乘项 建立线性归回模型。
   使用 因子变量 的语法格式，collgrad##i.race 表示
   在模型中既包括 collgrad 与 race 变量，
   还包括 collgrad 与 race 变量的 交乘项。
   stata 中的回归命令和结果如下所示：
   */
   
   reg wage ttl_exp tenure collgrad##i.race
   
   /*
   回归结果显示：大学毕业与黑种人的交乘项 collgrad#black 
   对妇女的小时工资的影响作用是显著的。我们想进一步了解 
   大学毕业(collgrad) 与 种族(race) 交乘项的 
   各个类别 对 妇女的小时工资 (wage) 的边际效应分别是多少。
   于是，我们使用 margins 附加 atmeans 的选项
   就可以计算 collgrad#black 各个类别
   在 所有变量的均值处 的 边际效应。
   stata 命令和结果如下所示：
   */
   
   margins collgrad#i.race, atmeans
   
   /*
   margins 命令的计算结果显示：not college grad#white 
   对妇女的小时工资的边际效应为 7.461，
   表明当其他变量处于均值水平时，
   白种人且没有大学毕业的妇女的小时工资的平均值为 7.461；
   对交乘项其他类别的边际效应的解释类似。
   
   我们使用 marginsplot 命令将计算结果用图的形式表示。
   stata 命令如下所示：
   */
   
   marginsplot
   
   /*
   大学毕业(collgrad) 对 妇女的小时工资 (wage) 的边际效应
   会受到 种族(race)的影响，因此，我们还想进一步了解
   在 不同种族类别下(race)，大学毕业(collgrad) 对 
   妇女的小时工资 (wage) 的边际效应分别是多少。
   于是，我们使用 margins 附加 dydx、at 与 atmeans 的选项
   来实现这个计算 。
   stata 命令和结果如下所示：
   */
   
   margins, dydx(collgrad) at(race=(1 2 3)) atmeans
   
   /*
   margins 命令的计算结果显示：当妇女为白种人时，
   collgrad=1 对妇女的小时工资的边际效应
   比 collgrad=0 对妇女的小时工资的边际效应高 2.604；
   对交乘项其他类别的边际效应的解释类似。
   同样的，我们使用 marginsplot 命令将计算结果用图的形式表示。
   stata 命令如下所示：
   */
   
   marginsplot
   
*- 3.2.2 类别变量与连续型变量交乘
   /*
   使用 stata 的自带数据 nlsw88.dta (1988年美国妇女小时工资)，
   以 wage (妇女的小时工资) 作为被解释变量、
   以 ttl_exp (总工作年限)、tenure (当前工作年限)、 
   collgrad (是否大学毕业) 、 union (是否工会成员)、 hours (每天工作小时数)、
   union (是否工会成员) 与 hours (每天工作小时数) 的 交乘项 作为解释变量 
   建立线性归回模型。使用 因子变量 的语法格式，
   i.union##c.hours 表示在模型中既包括 union 与 hours 变量，
   还包括 union 与 hours 变量的 交乘项。
   stata 中的回归命令和结果如下所示：
   */
   
   reg wage ttl_exp tenure collgrad i.union##c.hours
   
   /*
   回归结果显示union#c.hours 的系数值为 -0.103 并且在1%的水平上显著，
   表明 hours (每天工作小时数) 对 wage (妇女的小时工资) 的边际效应
   会受到 union (是否工会成员) 的影响；
   union (是否工会成员) 对 wage (妇女的小时工资) 的边际效应
   也会受到 hours (每天工作小时数) 的影响。

   我们使用 margins 命令附加 dydx 选项与 at 选项
   来计算当妇女为工会成员或非工会成员时，
   hours 对 wage 的边际效应分别为多少。
   stata 中的命令和结果如下所示：
   */
   
   margins, dydx(hour) at(union=(0 1))
   
   /*
   计算结果表明：当妇女为工会成员时，每天工作小时数增加 1 个单位，
   则小时工资下降-0.073个单位；当妇女为非工会成员时，
   每天工作小时增加 1 个单位，则小时工资增加 0.029 个单位。

   我们使用 marginsplot 命令将计算结果用图的形式表示。
   stata 命令如下所示：
   */
   
   marginsplot
   
   /*
   我们还想计算当妇女每天工作小时数不同时，
   union 对 wage 的边际效应分别为多少。
   可以使用 margins 命令附加 dydx 选项与 at 选项。
   stata 中的命令和结果如下所示：
   */
   
   margins, dydx(union) at(hours=(1(5)80))
   
   /*
   为了更直观的显示结果，我们使用 marginsplot 命令进行绘图。
   stata 命令如下所示：
   */
   
   marginsplot
   
   /*
   计算结果表明：相对于非工会成员对妇女小时工资的边际效应，
   随着hours增加，工会成员对妇女小时工资的边际效应逐渐减小；
   但当hours增加至41之后，边际效应在统计上不显著。
   */
   
*- 3.2.3 连续型变量与连续型变量交乘

   /*
   使用 stata 的自带数据 auto.dta (1978年美国汽车数据)，
   以 price (汽车价格) 作为被解释变量、
   以 mpg (每加仑汽油能够行驶的英里数)、weight (汽车重量)、 
   foreign (是否进口车)、 
   mpg (每加仑汽油能够行驶的英里数) 与 weight (汽车重量) 的 交乘项 
   作为解释变量建立线性归回模型。使用 因子变量 的语法格式，
   c.mpg##c.weight 表示在模型中既包括 mpg 与 weight 变量，
   还包括 mpg 与 weight 变量的 交乘项。
   stata 中的回归命令和结果如下所示：
   */
   
   sysuse "auto.dta", clear
   
   reg price foreign c.mpg##c.weight
   
   /*
   回归结果显示 c.mpg#c.weight 的系数值为 -0.118 并且在10%的水平上显著，
   表明该交乘项对 price 具有显著影响作用。 
   mpg (每加仑汽油能够行驶的英里数) 对 price (汽车价格) 的边际效应
   会受到 weight (汽车重量) 的影响；
   weight (汽车重量) 对 price (汽车价格) 的边际效应
   也会受到 mpg (每加仑汽油能够行驶的英里数) 的影响。
   
   为了进一步了解当 weight (汽车重量) 取不同数值时，
   mpg (每加仑汽油能够行驶的英里数) 对 price (汽车价格) 的边际效应分别是多少，
   我们可以使用 margins 命令附加 dydx 选项与 at 选项来计算。
   stata 中的命令和结果如下所示：
   */
   
   sum weight  //查看 weight 的基本统计量
   
   margins, dydx(mpg) at(weight=(1760(200)4840))
   
   /*
   为了使结果更加直观的显示出来，
   可使用 marginsplot 命令进行绘图。
   stata 命令和结果如下所示：
   */
   
   marginsplot
   
   /*
   计算结果表明：随着weight增加，mpg对price的边际效应逐渐减小,
   但在统计上不显著。
   
   相似的，当 mpg (每加仑汽油能够行驶的英里数) 取不同数值时，
   weight (汽车重量) 对 price (汽车价格) 的边际效应
   也可以使用 margins 命令附加 dydx 选项与 at 选项来计算。
   stata 中的命令和结果如下所示：
   */
   
   sum mpg   //查看 mpg 的基本统计量
   
   margins, dydx(weight) at(mpg=(12(2)41))
   
   /*
   为了使结果更加直观的显示出来，
   可使用 marginsplot 命令进行绘图。
   stata 命令和结果如下所示：
   */
   
   marginsplot
   
   
   /*
   计算结果表明：当mpg小于26时，随着mpg增加，
   weight对price的边际效应逐渐减小,且在统计上显著；
   当mpg大于26时，weight对price的边际效应逐渐减小在统计上不显著。
   */
   
   
*- 3.3 非线性模型(Logit Model)案例
   
   /*
   使用 stata 的自带数据 auto.dta (1978年美国汽车数据)，
   以 foreign (是否进口车) 作为被解释变量、
   以 mpg (每加仑汽油能够行驶的英里数)、
   weight (汽车重量) 作为解释变量 
   建立线性归回模型。
   stata 中的回归命令和结果如下所示：
   */
   
   sysuse "auto.dta", clear
   
   logit foreign mpg weight
   
   /*
   回归结果显示 mpg 与 weight 的系数值为负并且在统计上显著，
   表明当车辆的 mpg 与 weight 增加时，该车辆是进口车的概率减小。
   但从系数值上无法直接看出 mpg 与 weight 
   对 车辆是进口车的概率 的边际效应。
   于是，我们可以使用 margins 命令附加 dydx 选项来进行计算。
   stata 中的命令和结果如下所示：
   */
   
   margins, dydx(mpg)
   
   /*
   计算结果显示：当 mpg 增加 1 个单位时，
   车辆为进口车的概率减少 1.97%；
   当 weight 增加 1 个单位时，车辆为进口车的概率减少 0.04%。
   */
   
  