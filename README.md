# 杨柳
杨柳完成的任务提交到该项目中

- `2017.11.2`  使用这个命令可以查看：`search Marginal effects in log-transformed models, all`

- `2017/10/31 21:58`   新增参考资料。   

  在 command 窗口中输入 `net describe st0497` 查看。   
  SJ17-3 st0497. Stata tip 128: Marginal effects in log-transformed models: A trade application


- `2017/10/23 16:13` 【边际效应】任务中的参考资料

  - [包含离散变量与连续变量交乘项的Logit模型：估计、图示和解释](https://blog.stata.com/2016/07/12/effects-for-nonlinear-models-with-interactions-of-discrete-and-continuous-variables-estimating-graphing-and-interpreting/)（英文标题： Effects of nonlinear models with interactions of discrete and continuous variables: Estimating, graphing, and interpreting）