  ******************
  *    因子变量    *
  ******************

  *-1. 问题背景
  /*
  以研究妇女工资的决定因素为例，
  范例数据中有一个名为 race 的变量，
  取值情况如下
  */
  
  *-使用范例数据
    sysuse nlsw88, clear
  
  *-查看race的Value label名称
    des2 race 
  
  *-数字-文字对应表
	label list racelbl  
  
  *-频数分布表
	tab race   
	
  *-不使用因子变量的命令
    gen white = (race==1) 
    reg wage white tenure age collgrad
	
  *-使用因子变量的命令
    reg  wage  1.race  tenure  age  collgrad
	
	
  *-2. 因子变量的基本语法规则
  /*
  i.varname

  i.varname##i.varname
  */
   
   
	
  *-3. 回归中的基本引用方式
    reg  wage  i.race  tenure  age  collgrad
	
	
  *-4. 基准组的设定问题
  /*
  从上述回归结果中可以看到，
  race 中仅有 black 和 other 两个虚拟变量。
  这是由于stata默认将 race 中的第一个类别 white 作为基准组，
  目的在于防止完全共线性。
  若 varname 有N个类别，
  则 i.varname 会自动产生N-1个虚拟变量，
  以 varname 的第一类作为 基准组 。
  */
  
  *-5. 因子变量的交乘项
  *- 在回归模型中检验种族和婚姻状况相互交乘后对妇女工资的影响作用
  
  *-查看race的Value label名称
    des2 race 

  *-数字-文字对应表
    label list racelbl
	
  *-查看married的Value label名称
	des2 married
  
  *-数字-文字对应表
    label list marlbl
  
  *-回归
  *-类别变量与类别变量相互交乘
    reg wage i.race##i.married tenure  age  collgrad, noheader

  *-类别变量与连续型变量相互交乘
	reg wage i.married##c.tenure  race  age  collgrad, noheader
	
	
  *-6. 输出回归结果时的问题
  *-回归
    reg wage i.race##i.married tenure  age  collgrad, noheader
	
  *-储存结果
    est store r1
	
  *-输出结果
    esttab r1 using D:/Table_1.csv, nogap replace
	
  *-输出结果（删除估计系数值为0的行）
    esttab r1 using D:/Table_2.csv, nogap drop() replace
    
	
  *-7. 交乘项的边际效应（margins与marginsplot命令）
  
  *-类别变量与连续型变量相互交乘
  *-回归
    reg wage i.married##c.tenure  race  age  collgrad, noheader
	
  *-交乘项的边际效应
	margins married, dydx(tenure) pwcompare
	
  *-类别变量与类别变量相互交乘  
  *-回归
    reg wage i.race##i.married tenure  age  collgrad, noheader
	
  *-交乘项的边际效应
    margins married, dydx(race) pwcompare
	
  *-绘图（marginsplot命令）
  *-估计边际效应
    reg wage i.married##c.tenure  race  age  collgrad, noheader
	margins married, dydx(tenure) pwcompare
	
  *-绘图
    marginsplot, scale(0.8)
	
  *-输出图片
    graph export "D:/Figure_1.png", replace
	
	
	
