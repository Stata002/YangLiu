  ******************
  *    因子变量    *
  ******************

  *-1. 问题背景
    /*
    以研究妇女工资的决定因素为例，
    范例数据中有一个名为 race 的变量，
    取值情况如下
    */
  
  *-使用范例数据
    sysuse nlsw88, clear
  
  *-查看race的Value label名称
    des2 race 
  
  *-数字-文字对应表
	  label list racelbl  
  
  *-频数分布表
	  tab race   
	
  *-不使用因子变量的命令
    gen white = (race==1) 
    reg wage white tenure age collgrad
	
  *-使用因子变量的命令
    reg  wage  1.race  tenure  age  collgrad
	
	
  *-2. 因子变量的基本语法规则
    /*
    i.varname
  
    i.varname##i.varname
    
	*-因子变量的五种运算符：
    -----------------------------------------------
    运算符  描述
    -----------------------------------------------
    i.    确定指示变量（变量为类别变量）的一元运算符
    c.    确定指示变量（变量为连续型变量）的一元运算符
    o.    省略一个变量或一个指示变量的一元运算符
    #     确定交乘项的二元运算符
    ##    确定因子变量的交乘项的二元运算符
    -----------------------------------------------
    注：  
    (a) 由因子变量运算符生成的指示变量和交乘项是实际存在的变量，
	    它们与变量列表中的变量是一样的，但是在数据表中不显示出来；  
    (b) 类别变量的值必须是非负整数，范围介于0至32，也包括740；  
    (c) 因子变量有时可以与时间序列的运算符L.和F.组合在一起。
	  */   
   
	
  *-3. 回归中的基本引用方式
    reg  wage  i.race  tenure  age  collgrad
	
	
  *-4. 基准组的设定问题
    /*
    *-从上述回归结果中可以看到，
      race 中仅有 black 和 other 两个虚拟变量。
      这是由于stata默认将 race 中的第一个类别 white 作为基准组，
      目的在于防止完全共线性。
    *-若 varname 有N个类别，
      则 i.varname 会自动产生N-1个虚拟变量，
      以 varname 的第一类作为 基准组 。
	  *-若想改变基准组的设定，可使用 ib. 或 b. 的前缀	
      --------------------------------------------------------------
	    基准组运算符    描述
      --------------------------------------------------------------
      ib*.           使用 * 作为基准组，* 为变量的值
      ib(#*).        使用变量值中的第 * 位排序的值作为基准组
      ib(first).     使用变量的最小值作为基准组（该项为stata默认选项）
      ib(last).      使用变量的最大值作为基准组
      ib(freq).      使用变量值的频数最大的作为基准组
      ibn.           没有基准组
      --------------------------------------------------------------
      注：
      (a) i 可以省略不写。例如：ib2.group 与 b2.group 的写法等价；
      (b) ib(#2). 指使用变量值中的第二位排序的值作为基准组。
      (c) 如果想在线性回归模型中将group变量中值为3的类别设置为基准组，
	      则命令可写为：regress y i.sex ib3.group
	    */
  
  *-5. 因子变量的交乘项
  *- 在回归模型中检验种族和婚姻状况相互交乘后对妇女工资的影响作用
  
  *-查看race的Value label名称
    des2 race 

  *-数字-文字对应表
    label list racelbl
	
  *-查看married的Value label名称
	  des2 married
  
  *-数字-文字对应表
    label list marlbl
  
  *-回归
  *-类别变量与类别变量相互交乘
    reg wage i.race##i.married tenure  age  collgrad, noheader

  *-类别变量与连续型变量相互交乘
	  reg wage i.married##c.tenure  race  age  collgrad, noheader

	
  *-6. 输出回归结果时的问题
  *-回归
    reg wage i.race##i.married tenure  age  collgrad, noheader
	
  *-储存结果
    est store r1
	
 	/*
	打开 `D:/Table_1` 之后，我们发现有很多变量的系数值为0
	*-造成某些变量的系数值为0的原因有以下：
     *一方面是由于有些虚拟变量作为了基准组，
	  例如： `1.race`，`0.married` 与 `1.race#0.married`，
	  stata默认将它们作为基准组；
     *另一方面是由于有些交乘项中的其中一个因子变量是基准组，
	  其它变量与这个作为基准组的因子变量交乘后的交乘项就被忽略了，
	  所以其估计系数值和标准误就会缺失，
	  例如： `1.race#1.married`，`2.race#0.married` 与 `3.race#0.married`；  
     *此外，有些交乘项的观测值的个数很少，小于模型中变量的个数，
	  这些观测值不参与回归过程，因此就缺失这些交乘项的估计系数值和标准误，
	  例如 `3.race#0.married` 的观测值仅有8个，而模型中变量有11个。

    *-此时，可以使用 `esttab` 命令的 `drop()` 选项来屏蔽这些系数的显示，
	  还可以使用 `nobase` 和 `noomit` 的选项，stata命令和结果如下所示：	
	*/
		
		
  *-查看race与married交乘项的各类别
	egen racemarried = group(race married), label
	egen racemarried_num = group(race married)
  
  *-race与married交乘项的各类别的频数
	tab racemarried
	tab racemarried_num
		
  *-输出结果
    esttab r1 using D:/Table_1.csv, nogap replace
	
  *-输出结果（不显示基准组和忽略组的系数）
    esttab r1 using D:/Table_2.csv, nogap           ///
	         drop(1.race 0.married 1.race#0.married   ///
	              1.race#1.married 2.race#0.married   ///
		            3.race#0.married) replace
  *-输出结果（不显示基准组和忽略组的系数，使用nobase与noomit的选项）
	  esttab r1 using D:/Table_3.csv, nogap          /// 
	       nobase noomit replace                          
		   
	
  *-7. 交乘项的边际效应
  
  *-类别变量与连续型变量相互交乘
  *-回归
    reg wage i.married##c.tenure  race  age  collgrad, noheader
	
  *-交乘项的边际效应
	  margins married, dydx(tenure) pwcompare

	
  *-类别变量与类别变量相互交乘  
  *-回归
    reg wage i.race##i.married tenure  age  collgrad, noheader
	
  *-交乘项的边际效应
    margins married, dydx(race) pwcompare
	  margins race, dydx(married) pwcompare
	
  *-绘图（marginsplot命令）
  *-估计边际效应
    reg wage i.married##c.tenure  race  age  collgrad, noheader
	  margins married, dydx(tenure) pwcompare
	
  *-绘图
    marginsplot, scale(0.8)
	
  *-输出图片
    graph export "D:/Figure_1.png", replace